const io = require('socket.io')()

const avaliableNames = ['Alberto', 'Igor', 'Tom', 'Ivan', 'Valentina']
let connectedClients = []
let i = 1

io.on('connection', (client) => {
    console.log('client opened connection:', new Date())
    console.log(client.id)

    client.emit('do_you_have_a_name')

    client.on('no', () => {

        console.log('********************  no')

        if (avaliableNames.length > 0) {
            connectedClients.unshift({
                client,
                name: avaliableNames.pop()
            })
        } else {
            connectedClients.unshift({
                client,
                name: `Guest_${i++}`
            })
        }

        client.emit('name', connectedClients[0].name)
    })

    client.on('yes', name => {

        console.log('********************  yes')

        connectedClients = connectedClients.filter(cc => cc.name !== name)

        connectedClients.unshift({
            client,
            name
        })

        client.emit('name', connectedClients[0].name)
    })

    client.on('message', () => {

        console.log(`message from ${connectedClients.filter(cc => cc.client === client)[0].name}`)

        client.broadcast.emit('pull')

    })
})

const port = 8000
io.listen(port)
console.log('listening on port ', port)

/*
client keys
****************************
nsp
server
adapter
id
client
conn
rooms
acks
connected
disconnected
handshake
fns
flags
_rooms
buildHandshake
emit
in
to
write
send
packet
join
leave
leaveAll
onconnect
onpacket
onevent
ack
onack
ondisconnect
onerror
onclose
error
disconnect
compress
binary
dispatch
use
run
_events
_eventsCount
_maxListeners
setMaxListeners
getMaxListeners
addListener
on
prependListener
once
prependOnceListener
removeListener
off
removeAllListeners
listeners
rawListeners
listenerCount
eventNames


*/
