import React from 'react'
import LoaderSpinner from 'react-loader-spinner'

export const Spinner = () => (
    <div className="spinner">
        <LoaderSpinner
            type="Plane"
            color="#00BFFF"
            height="100"
            width="100"
        />
    </div>
)
