import React from 'react'
import * as moment from 'moment'
import HtmlEntities from 'html-entities'

import { purify } from '../../utils/misc'

const entities = new HtmlEntities.XmlEntities()

export const IncomingMessage = ({ msg }) => (
    <div className="incoming-message">
        <span>
            {purify(entities.decode(msg.author))}
        </span>
        <p>
            {purify(entities.decode(msg.message))}
        </p>
        <span>
            {moment(msg.timestamp).format('lll')}
        </span>
    </div>
)
