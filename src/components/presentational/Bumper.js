import React from 'react'

export const Bumper = ({ sizeClass }) => (
    <div className={sizeClass}>&nbsp;</div>
)