import React from 'react'
import * as moment from 'moment'
import HtmlEntities from 'html-entities'

import { purify } from '../../utils/misc'

const entities = new HtmlEntities.XmlEntities()

export const OutgoingMessage = ({ msg }) => (
    <div className="outgoing-message">
        <p>
            {purify(entities.decode(msg.message))}
        </p>
        <span>
            {moment(parseInt(msg.timestamp, 10)).format('lll')}
        </span>
    </div>
)
