import React from 'react'

import { scrollHelper } from './Messages'
import { scrollAllTheWayDown } from '../../utils/misc'

export const MessageSender = ({ socket, name, handleMessageSending }) => {

    let input

    return (
        <div className="lower">
            <div className="lower-wrapper">
                <input
                    ref={node => { input = node }}
                    type="text"
                    placeholder="Message"
                    onKeyUp={event => {
                        event.preventDefault()
                        if (event.keyCode === 13) {
                            handleMessageSending(socket, name, input.value)
                                .then(whatever => scrollAllTheWayDown(scrollHelper.upperDiv))
                            input.value = ''
                        }
                    }}
                />
                <button
                    onClick={() => {
                        handleMessageSending(socket, name, input.value)
                            .then(whatever => scrollAllTheWayDown(scrollHelper.upperDiv))
                        input.value = ''
                    }}
                    type="button"
                >
                    Send
                </button>
            </div>
        </div>
    )
}
