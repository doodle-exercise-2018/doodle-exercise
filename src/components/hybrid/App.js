import React, { Component, Fragment } from 'react'

import { Messages } from '../containers/Messages'
import { MessageSender } from '../containers/MessageSender'
import { Spinner } from '../presentational/Spinner'

import { loadMessages } from '../../utils/api'
import { BASE_URL, TOKEN } from '../../constants/api'
import { loading } from '../../actions/loadingMessagesFlag'
import { setIncomingMessages } from '../../actions/incomingMessages'
import { scrollHelper } from './Messages'
import { scrollAllTheWayDown } from '../../utils/misc'

export class App extends Component {

    async componentDidMount() {
        const messages = await loadMessages(BASE_URL, TOKEN)

        if (!messages) return

        const { dispatch } = this.props

        dispatch(loading(false))
        dispatch(setIncomingMessages(messages))

        scrollAllTheWayDown(scrollHelper.upperDiv)
    }

    render() {

        const { loadingMessagesFlag } = this.props

        return (
            <div className="wrapper">

                {
                    loadingMessagesFlag ?
                        <Spinner/>
                        :
                        <Fragment>
                            <Messages/>
                            <MessageSender/>
                        </Fragment>
                }

            </div>
        )
    }
}
