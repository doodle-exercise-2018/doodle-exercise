import React from 'react'

import { IncomingMessage } from '../presentational/IncomingMessage'
import { OutgoingMessage } from '../presentational/OutgoingMessage'
import { Bumper } from '../presentational/Bumper'

export const scrollHelper = {}

export const Messages = ({ incomingMessages, outgoingMessages }) => {

    const allMessages = [...incomingMessages, ...outgoingMessages]

    allMessages.sort((left, right) => left.timestamp - right.timestamp)

    const bumpers = []
    for (let i = 0; i < allMessages.length - 1; i++) {

        if (allMessages[i].outgoingFlag === allMessages[i + 1].outgoingFlag) {

            bumpers.push('size-8')

        } else {

            bumpers.push('size-16')
        }
    }

    let mixture = []
    for (let i = 0; i < allMessages.length - 1; i++) {

        mixture.push(allMessages[i])
        mixture.push(bumpers[i])
    }
    mixture.push(allMessages[allMessages.length - 1])
    
    mixture = mixture.filter(m => m)

    return (
        <div className="upper" ref={node => { scrollHelper.upperDiv = node }}>
            {
                mixture.map(m =>

                    typeof m === 'string' ?

                        <Bumper sizeClass={m} key={Math.random()}/>

                    : m.outgoingFlag ?

                        <OutgoingMessage msg={m} key={m._id}/>
                    :

                        <IncomingMessage msg={m} key={m._id}/>
                )
            }
        </div>
    )

}
