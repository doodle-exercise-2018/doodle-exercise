import { connect } from 'react-redux'

import * as Presentational from '../hybrid/Messages'

const mapStateToProps = state => ({
    incomingMessages: state.incomingMessages,
    outgoingMessages: state.outgoingMessages,
})

export const Messages = connect(mapStateToProps)(Presentational.Messages)
