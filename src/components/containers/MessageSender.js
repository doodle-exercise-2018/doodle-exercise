import { connect } from 'react-redux'

import * as Presentational from '../hybrid/MessageSender'

import { addOutgoingMessage } from '../../actions/outgoingMessage'
import { sendMessage } from '../../utils/api'
import { BASE_URL, TOKEN } from '../../constants/api'

const mapStateToProps = state => ({
    socket: state.socket,
    name: state.name
})

const mapDispatchToProps = dispatch => ({
    handleMessageSending: (socket, name, inputValue) => sendMessage(BASE_URL, TOKEN, name, inputValue)

        .then(responseMessage => {

            if (!responseMessage) return

            socket.emit('message')

            responseMessage.outgoingFlag = true

            return dispatch(addOutgoingMessage(responseMessage))
        })
        .catch(err => console.log(err))
})

export const MessageSender = connect(mapStateToProps, mapDispatchToProps)(Presentational.MessageSender)
