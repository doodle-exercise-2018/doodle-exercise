import { connect } from 'react-redux'

import * as Hybrid from '../hybrid/App'

const mapStateToProps = state => ({
    loadingMessagesFlag: state.loadingMessagesFlag
})

export const App = connect(mapStateToProps)(Hybrid.App)
