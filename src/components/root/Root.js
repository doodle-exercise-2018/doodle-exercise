import React from 'react'
import { Provider } from 'react-redux'

import { configureStore } from '../../state/configureStore'
import { preloadedState } from '../../state/preloadedState'
import { setName } from '../../actions/setName'
import { loadMessages } from '../../utils/api'
import { BASE_URL, TOKEN } from '../../constants/api'
import { addNewIncomingMessages } from '../../actions/incomingMessages'
import { loadPartialState, savePartialState, scrollAllTheWayDown } from '../../utils/misc'
import { scrollHelper } from '../hybrid/Messages'

import { App } from '../containers/App'

const startingState = loadPartialState(preloadedState)
const store = configureStore(startingState)

store.subscribe(() => savePartialState(store.getState(), ['name', 'outgoingMessages']))

preloadedState.socket.on('do_you_have_a_name', () => {

    console.log('*********************    do_you_have_a_name')

    const name = store.getState().name

    if (name === '') {
        preloadedState.socket.emit('no')
    } else {
        preloadedState.socket.emit('yes', name)
    }
})
preloadedState.socket.on('name', name => console.log('************** name', name) || store.dispatch(setName(name)))
preloadedState.socket.on('pull', async () => {

    const messages = await loadMessages(BASE_URL, TOKEN)

    if (!messages) return

    store.dispatch(addNewIncomingMessages(messages))

    scrollAllTheWayDown(scrollHelper.upperDiv)
})

export default () => (
    <Provider store={store}>
        <App/>
    </Provider>
)
