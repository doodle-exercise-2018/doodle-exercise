export const UPDATE_LOADING_MESSAGES_FLAG = 'UPDATE_LOADING_MESSAGES_FLAG'

export const loading = flag => ({
    type: UPDATE_LOADING_MESSAGES_FLAG,
    flag
})
