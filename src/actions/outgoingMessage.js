export const ADD_OUTGOING_MESSAGE = 'ADD_OUTGOING_MESSAGE'

export const addOutgoingMessage = responseMessage => ({
    type: ADD_OUTGOING_MESSAGE,
    responseMessage
})
