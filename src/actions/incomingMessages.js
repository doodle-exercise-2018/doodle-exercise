export const SET_INCOMING_MESSAGES = 'SET_INCOMING_MESSAGES'
export const ADD_NEW_INCOMING_MESSAGES = 'ADD_NEW_INCOMING_MESSAGES'

export const setIncomingMessages = messages => ({
    type: SET_INCOMING_MESSAGES,
    messages
})

export const addNewIncomingMessages = messages => ({
    type: ADD_NEW_INCOMING_MESSAGES,
    messages
})
