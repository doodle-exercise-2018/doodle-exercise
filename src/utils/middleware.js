import {
    ADD_NEW_INCOMING_MESSAGES,
    SET_INCOMING_MESSAGES
} from '../actions/incomingMessages'

export const incomingMessagesHelper = store => next => action => {

    if ([ADD_NEW_INCOMING_MESSAGES, SET_INCOMING_MESSAGES].indexOf(action.type) > -1) {

        action.__state = store.getState()
    }

    return next(action)
}