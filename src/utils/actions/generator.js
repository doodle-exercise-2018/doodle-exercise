const fs = require('fs')

const config = {
    suffix: 'name',
    actions: [
        'set',
    ]
}

const suffix = config.suffix
    .split(' ')
    .map(s => s.trim().toUpperCase()).join('_')

const actions = config.actions
    .map(a => a.split(' ').map(s => s.trim().toUpperCase()).join('_'))
    .map(a => [a, suffix].join('_'))

const actionAssignments = actions
    .map(a => `export const ${a} = '${a}'`)

const actionCreators = config.actions
    .map(a => a.split(' ')
        .map(s => s.trim())
        .map((s, i) => i === 0 ? s : upperFirstLowerRest(s))
        .join('')
    )
    .map((s, i) => `
export const ${s} = * => ({
    type: ${actions[i]},
    *
})`)

const str = actionAssignments.join('\n') + '\n' + actionCreators.join('\n') + '\n'

fs.writeFileSync('actions.js', str)

function upperFirstLowerRest(str) {

    const rest = str.slice(1).toLowerCase();
    const first = str.slice(0, 1).toUpperCase();

    return first + rest;
}
