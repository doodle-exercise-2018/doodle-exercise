import DOMPurify from 'dompurify'

export const range = (from, to) => {

    const arr = []

    for (let i = from; i <= to; i++) {
        arr.push(i)
    }

    return arr
}

export const loadPartialState = preloadedState => {

    try {

        const jsonStr = localStorage.getItem('state')

        if (jsonStr === null) {

            return preloadedState
        }

        const partialState = JSON.parse(jsonStr)

        const keys = Object.keys(partialState)

        keys.forEach(key => preloadedState[key] = partialState[key])

        return preloadedState

    } catch (err) {

        return preloadedState
    }
}

export const savePartialState = (state, keys) => {

    try {

        const partialState = {}

        keys.forEach(key => partialState[key] = state[key])

        const jsonStr = JSON.stringify(partialState)

        localStorage.setItem('state', jsonStr)

    } catch (err) {

        console.log(err)
    }
}

export const purify = dirty => {

    const config = { FORBID_TAGS: ['svg'] }
    const clean = DOMPurify.sanitize(dirty, config)

    console.log('dirty', dirty)
    console.log('clean', clean)
    console.log('removed', DOMPurify.removed)

    if (DOMPurify.removed.length > 0) {
        return clean
    }
    else {
        return dirty
    }
}

export const scrollAllTheWayDown = element => element.scrollTop = element.scrollHeight - element.clientHeight
