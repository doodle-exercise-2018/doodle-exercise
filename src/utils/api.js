import fetch from 'cross-fetch'

export const handleFetchErrors = (response) => {

    if (!response.ok) throw Error(response.statusText)

    return response
}

const sinceLately = Date.now() - 45 * 60 * 1000

export const loadMessages = (url, token, since = sinceLately, limit = 200) =>
    fetch(`${url}?token=${token}&since=${since}&limit=${limit}`)
        .then(handleFetchErrors)
        .then(response => response.json())
        .then(messages => console.log(messages) || messages)
        .catch(err => console.error(err))

export const sendMessage = (url, token, name, message) => {

    const body = {
        message,
        author: name
    }

    const config = {
        method: 'post',
        body:    JSON.stringify(body),
        headers: {
            token,
            'Content-Type': 'application/json'
        }
    }

    return fetch(url, config)
        .then(handleFetchErrors)
        .then(response => response.json())
        .then(message => console.log(message) || message)
        .catch(err => console.error(err))
}