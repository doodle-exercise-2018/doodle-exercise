import { ADD_OUTGOING_MESSAGE } from '../actions/outgoingMessage'

export const outgoingMessages = (state = [], action) => {

    switch (action.type) {

        case ADD_OUTGOING_MESSAGE:
            return [...state, action.responseMessage]

        default:
            return state;
    }
}
