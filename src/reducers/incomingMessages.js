import {
    SET_INCOMING_MESSAGES,
    ADD_NEW_INCOMING_MESSAGES
} from '../actions/incomingMessages'

const union_1 = (action) => {

    let messages = []
    let outgoingMessages = action.__state.outgoingMessages

    action.messages.forEach(msg => {

        let noOutgoingDuplicates = !outgoingMessages.some(outMsg => outMsg._id === msg._id)

        if (noOutgoingDuplicates) {

            messages.push(msg)
        }
    })

    return messages
}

const union_2 = (state, action) => {

    let oldMessages = [...state]
    let outgoingMessages = action.__state.outgoingMessages

    action.messages.forEach(msg => {

        let noIncomingDuplicates = !oldMessages.some(oldMsg => oldMsg._id === msg._id)
        let noOutgoingDuplicates = !outgoingMessages.some(outMsg => outMsg._id === msg._id)

        if (noIncomingDuplicates && noOutgoingDuplicates) {

            oldMessages.push(msg)
        }
    })

    return oldMessages
}

export const incomingMessages = (state = [], action) => {

    switch (action.type) {

        case SET_INCOMING_MESSAGES:
            return union_1(action)

        case ADD_NEW_INCOMING_MESSAGES:
            return union_2(state, action)

        default:
            return state;
    }
}