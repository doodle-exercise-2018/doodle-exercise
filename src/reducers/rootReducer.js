import { combineReducers } from 'redux'

import { loadingMessagesFlag } from './loadingMessagesFlag'
import { incomingMessages } from './incomingMessages'
import { socket } from './socket'
import { name} from './name'
import { outgoingMessages } from './outgoingMessages'

export const rootReducer = combineReducers({
    name,
    socket,
    loadingMessagesFlag,
    incomingMessages,
    outgoingMessages
})
