import { UPDATE_LOADING_MESSAGES_FLAG } from '../actions/loadingMessagesFlag'

export const loadingMessagesFlag = (state = true, action) => {

    switch (action.type) {

        case UPDATE_LOADING_MESSAGES_FLAG:
            return action.flag

        default:
            return state;
    }
}
