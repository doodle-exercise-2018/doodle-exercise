import { createStore, applyMiddleware } from 'redux'
import { createLogger } from 'redux-logger'
import { composeWithDevTools } from 'redux-devtools-extension'

import { rootReducer } from '../reducers/rootReducer'
import { incomingMessagesHelper } from '../utils/middleware'

const loggerMiddleware = createLogger()

export const configureStore = (preloadedState) => createStore(

    rootReducer,
    preloadedState,

    composeWithDevTools(
        applyMiddleware(loggerMiddleware, incomingMessagesHelper)
    )
)
