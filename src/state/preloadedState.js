import socketOpen from 'socket.io-client'

const  socket = socketOpen('http://localhost:8000')
console.log('socket.ids', socket.ids)

export const preloadedState = {
    name: '',
    socket,
    loadingMessagesFlag: true,
    incomingMessages: [],
    outgoingMessages: []
}
