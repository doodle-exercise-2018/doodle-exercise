## Installation

Go to root folder of the project and run:

### `npm install`

## Usage

Still being in root folder of the project, simply run:

### `npm start`

For application to work properly you will also need to start separate Socket.io server. 
The purpose of this server is to provide random names for connected users and also to 
deliver push notifications to these users. So, from terminal run:  

### `cd src/name_and_push_server/`

and 

### `node server.js`

Now open two or more different browsers and start chatting. Two different tabs in the same browser
won't work because application relies on Local Storage which is shared between tabs from same 
domain (the same user would appear in both tabs, which is not what we want).

Application is also deployed on Heroku, so feel free to visit 
(https://nameless-brushlands-70894.herokuapp.com/) if you prefer so.

Query parameters **since** and **limit** are set to **45 minutes** and **200 messages** 
respectively.

While testing the app you will also want to clear Local Storage from time to time. Please do so.

--------------------------------------------------
--------------------------------------------------
--------------------------------------------------
--------------------------------------------------

## Create React App Stuff



Application 

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
